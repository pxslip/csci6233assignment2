#Repository#
https://bitbucket.org/wskruse/csci6233assignment2

#Installation#
- Install [Android Studio](https://developer.android.com/sdk/index.html)
- Open this project in Android Studio
    - Do this by opening Android Studio, use the open command and open the project folder
- Use Run (ctrl+R) to start the application and select an Emulator or attached device

#Rationale#
The CS6233Chatter app was primarily designed to allow me to play with the [Firebase](https://www.firebase.com/). However, the concept, an application that members of a course or group can use to share both quick chats as well as a shared task list could be useful.

#Usage#
- Both the chat pane and tasks pane are shared among all users of the CS6233Chatter application
- Chat:
    - On the default screen you will see a list of all message posted to the CS6233Chatter Chat room
    - You can add a message to the list using the textfield and send button at the bottom of the chat screen
- Tasks
    - At the top you can select the Tasks option to see a list of all tasks added to the CS6233Chatter task list
    - You can add a new task to the list using the form at the bottom of the task view
    - You are able to update a task by clicking on the task title. This will load the details into the form at the bottom, and alter the create button to an update button. NOTE: There is currently no way to cancel an update process, but clicking the update button without altering the fields is equivalent.

#Testing#
A basic test suite was built using the Android Instrumentation tools, based on JUnit. Currently the tests simply ensure that all required views appear correctly in the application.

One can run the test suite from Android Studio by locating the class MainActivtyTest and using the Run command. This will launch an emulator, and run a set of basic tests.

##More Tests##
- [ ] Test the content of the ListViews/That there is content
- [ ] Test creation of tasks
- [ ] Test the sending of a chat message
- [ ] Test updating of a task

#To Do#
- [x] Allow for editing of tasks
- [x] Allow for completion of tasks
- [ ] Be able to cancel out of updating a task, without simply clicking update without editing
- [ ] Fix the highlight color, GW buff is really ugly in an Android app
- [ ] Allow users to create groups/new courses within the app to allow for segmentation
- [ ] Require users to register via FB/Google/Twitter to enforce a naming policy on users
- [ ] Allow for private tasks
- [ ] Allow for direct messaging
- [ ] Figure out how to use the test tools to select a new spinner value
- [ ] Remove the menu drop down as it is currently unused
