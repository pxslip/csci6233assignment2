package edu.gwu.csci6233.wskruse.csci6233assignment2.support;

/**
 * Created by wskruse on 11/4/15.
 */
public class Message {

    private String message;

    public Message() {

    }

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

}
