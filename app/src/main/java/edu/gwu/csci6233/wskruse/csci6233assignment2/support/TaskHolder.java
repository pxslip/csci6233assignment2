package edu.gwu.csci6233.wskruse.csci6233assignment2.support;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import edu.gwu.csci6233.wskruse.csci6233assignment2.R;

/**
 * Created by wskruse on 11/5/15.
 */
public class TaskHolder extends RecyclerView.ViewHolder {

    private TextView title;
    private TextView date;
    private CheckBox done;
    private View wrapper;


    public TaskHolder(View itemView) {
        super(itemView);
        wrapper = itemView;
        title = (TextView) itemView.findViewById(R.id.task_title);
        date = (TextView) itemView.findViewById(R.id.task_date);
        done = (CheckBox) itemView.findViewById(R.id.task_checkbox);
    }

    public TextView getTitleView() {
        return this.title;
    }

    public TextView getDateView() {
        return this.date;
    }

    public CheckBox getCheckBoxView() {
        return this.done;
    }

    public View getWrapper() {
        return this.wrapper;
    }
}
