package edu.gwu.csci6233.wskruse.csci6233assignment2;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources.Theme;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.ui.FirebaseRecyclerViewAdapter;

import java.util.Calendar;

import edu.gwu.csci6233.wskruse.csci6233assignment2.support.Message;
import edu.gwu.csci6233.wskruse.csci6233assignment2.support.MessageHolder;
import edu.gwu.csci6233.wskruse.csci6233assignment2.support.Task;
import edu.gwu.csci6233.wskruse.csci6233assignment2.support.TaskHolder;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "csci6233Assignment2";

    //refer to https://github.com/firebase/FirebaseUI-Android for creating bindings from Firebase to view


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        // Setup spinner that changes between the sections
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setAdapter(new SectionSpinnerAdapter(
                toolbar.getContext(),
                new String[]{
                        "Chatter",
                        "Tasks",
                }));

        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // When the given dropdown item is selected, show its contents in the
                // container view.
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, ListViewFragment.newInstance(position))
                        .commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static class SectionSpinnerAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        public SectionSpinnerAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                // Inflate the drop down using the helper's LayoutInflater
                view = mDropDownHelper.getDropDownViewInflater().inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(getItem(position));

            return view;
        }

        @Override
        public void setDropDownViewTheme(Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }

        @Override
        public Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class ListViewFragment extends Fragment {

        private static final String fbRef = "https://cs6233chatter.firebaseio.com";
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        private FirebaseRecyclerViewAdapter<Task, TaskHolder> mTaskAdapter;
        private Button mCreateTaskButton;
        private EditText mTitleEditText;
        private EditText mDateEditText;
        private RecyclerView mRecyclerView;
        private View.OnClickListener mCreateTaskClickListener;
        //The vars for the Chat fragment
        private FirebaseRecyclerViewAdapter<Message, MessageHolder> mChatAdapter;
        private EditText mChatMessageEditText;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ListViewFragment newInstance(int sectionNumber) {
            ListViewFragment fragment = new ListViewFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public ListViewFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Bundle args = getArguments();
            int section = (args != null) ? args.getInt(ARG_SECTION_NUMBER) : 0;
//            Firebase.getDefaultConfig().setPersistenceEnabled(true);
            Firebase.setAndroidContext(getActivity());
            final Firebase ref = new Firebase(fbRef);
            switch (section) {
                case 0:
                    //inflate the list view in fragment_chat, then set an adapter based on the position of the spinner
                    View chatView = inflater.inflate(R.layout.fragment_chat, container, false);
                    mRecyclerView = (RecyclerView) chatView.findViewById(R.id.chat_list);
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mRecyclerView.setAdapter(new FirebaseRecyclerViewAdapter<Message, MessageHolder>(Message.class, android.R.layout.two_line_list_item, MessageHolder.class, ref.child("chatter")) {
                        @Override
                        protected void populateViewHolder(MessageHolder viewHolder, Message model) {
                            viewHolder.getMessageView().setText(model.getMessage());
                        }
                    });
                    //let's try to hide the action button on the main layout
                    mChatMessageEditText = (EditText) chatView.findViewById(R.id.chat_message_text);
                    chatView.findViewById(R.id.chat_message_send).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //send the chat message here
                            ref.child("chatter").push().setValue(new Message(mChatMessageEditText.getText().toString()));
                            mChatMessageEditText.setText("");
                        }
                    });
                    return chatView;
                case 1:
                    //inflate the list view in fragment_tasks, then set an adapter based on the position of the spinner
                    View taskView = inflater.inflate(R.layout.fragment_tasks, container, false);
                    mRecyclerView = (RecyclerView) taskView.findViewById(R.id.task_list);
                    mDateEditText = (EditText) taskView.findViewById(R.id.tasks_new_task_date);
                    mTitleEditText = (EditText) taskView.findViewById(R.id.tasks_new_task_title);
                    mCreateTaskButton = (Button) taskView.findViewById(R.id.tasks_new_task_create_button);
                    mCreateTaskClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ref.child("tasks").push().setValue(new Task(mTitleEditText.getText().toString(), mDateEditText.getText().toString()));
                            mDateEditText.setText("");
                            mTitleEditText.setText("");
                        }
                    };
                    mCreateTaskButton.setOnClickListener(mCreateTaskClickListener);
                    mTaskAdapter = new FirebaseRecyclerViewAdapter<Task, TaskHolder>(Task.class, R.layout.checkbox_list_item, TaskHolder.class, ref.child("tasks")) {
                        @Override
                        protected void populateViewHolder(TaskHolder viewHolder, final Task model, final int position) {
                            viewHolder.getTitleView().setText(model.getTitle());
                            viewHolder.getDateView().setText(model.getDate());
                            final CheckBox checkbox = viewHolder.getCheckBoxView();
                            checkbox.setChecked(model.getDone());
                            checkbox.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    model.setDone(!model.getDone());
                                    checkbox.setChecked(model.getDone());
                                    mTaskAdapter.getRef(position).setValue(model);
                                }
                            });
                            viewHolder.getWrapper().setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //TODO add a listener for the clicks on the wrapper, this will trigger an update
                                    mDateEditText.setText(model.getDate());
                                    mTitleEditText.setText(model.getTitle());
                                    mCreateTaskButton.setText(R.string.task_update_button_text);
                                    mCreateTaskButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            model.setTitle(mTitleEditText.getText().toString());
                                            mTitleEditText.setText("");
                                            model.setDate(mDateEditText.getText().toString());
                                            mDateEditText.setText("");
                                            mTaskAdapter.getRef(position).setValue(model);
                                            //restore the original listener and text
                                            mCreateTaskButton.setText(R.string.task_create_button_text);
                                            mCreateTaskButton.setOnClickListener(mCreateTaskClickListener);
                                        }
                                    });
                                }
                            });
                        }

                    };
                    mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    mRecyclerView.setAdapter(mTaskAdapter);
                    View.OnClickListener dateListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DatePickerFragment dpFrag = new DatePickerFragment() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    StringBuilder b = new StringBuilder();
                                    b.append(year);
                                    b.append(".");
                                    b.append(monthOfYear);
                                    b.append(".");
                                    b.append(dayOfMonth);
                                    mDateEditText.setText(b.toString());
                                }
                            };
                            dpFrag.show(getActivity().getSupportFragmentManager(), "taskDatePicker");
                        }
                    };
                    mDateEditText.setOnClickListener(dateListener);
                    taskView.findViewById(R.id.tasks_new_task_set_date_button).setOnClickListener(dateListener);

                    return taskView;
                default:
                    Snackbar.make(container, "Something went very, very wrong...", Snackbar.LENGTH_SHORT).show();
                    break;
            }
            //TODO this is bad, very bad!! Return some sort of error view here instead
            return null;
        }
    }

    public static abstract class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Calendar c = Calendar.getInstance();
            return new DatePickerDialog(getActivity(), this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        }
    }

}
