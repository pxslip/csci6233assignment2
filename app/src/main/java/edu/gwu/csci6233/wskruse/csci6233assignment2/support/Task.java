package edu.gwu.csci6233.wskruse.csci6233assignment2.support;

/**
 * Created by wskruse on 11/5/15.
 */
public class Task {

    private String title;
    private String date;
    private boolean done;

    public Task() {

    }

    public Task(String title, String date) {
        this.title = title;
        this.date = date;
        this.done = false;
    }

    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return this.date;
    }
    public void setDate(String date) {
        this.date = date;
    }

    public boolean getDone() {
        return this.done;
    }
    public void setDone(boolean done) {
        this.done = done;
    }
}
