package edu.gwu.csci6233.wskruse.csci6233assignment2.support;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

/**
 * Created by wskruse on 11/4/15.
 */
public class MessageHolder extends RecyclerView.ViewHolder {

    private TextView message;

    public MessageHolder(View itemView) {
        super(itemView);
        message = (TextView) itemView.findViewById(android.R.id.text1);
    }

    public TextView getMessageView() {
        return this.message;
    }

}
