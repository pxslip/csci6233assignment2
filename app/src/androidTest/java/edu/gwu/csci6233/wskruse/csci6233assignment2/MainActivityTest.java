package edu.gwu.csci6233.wskruse.csci6233assignment2;

import android.support.v7.widget.RecyclerView;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.MediumTest;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * Created by wskruse on 11/8/15.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mMainActivity;
    private Spinner mSpinner;
    private View mDecorView;

    private static final int CHAT_SELECTION = 0;
    private static final int TASK_SELECTION = 1;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        //we want to be able to use keyboard events for testing
        setActivityInitialTouchMode(false);
        mMainActivity = getActivity();
        mSpinner = (Spinner) mMainActivity.findViewById(R.id.spinner);
        mDecorView = mMainActivity.getWindow().getDecorView();
    }

    @MediumTest
    public void testSpinner_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(CHAT_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.spinner));
    }

    @MediumTest
    public void testChatList_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(CHAT_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.chat_list));
    }


    @MediumTest
    public void testMessage_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(CHAT_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.chat_message_text));
    }

    @MediumTest
    public void testSendButton_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(CHAT_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.chat_message_send));
    }

    @MediumTest
    public void testTaskList_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(TASK_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.task_list));
    }

    @MediumTest
    public void testTasksNewTaskTitle_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(TASK_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.tasks_new_task_title));
    }

    @MediumTest
    public void testTasksNewTaskDate_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(TASK_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.tasks_new_task_date));
    }

    @MediumTest
    public void testTasksNewTaskSetDateButton_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(TASK_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.tasks_new_task_set_date_button));
    }

    @MediumTest
    public void testTasksNewTaskCreateButton_layout() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.changeSpinnerToSelection(TASK_SELECTION);
        ViewAsserts.assertOnScreen(mDecorView, mMainActivity.findViewById(R.id.tasks_new_task_create_button));
    }

    public void changeSpinnerToSelection(int selection) {
        mMainActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSpinner.requestFocus();
                mSpinner.setSelection(0);
            }
        });
        this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
        for(int i = 0; i < selection; i++) {
            this.sendKeys(KeyEvent.KEYCODE_DPAD_DOWN);
        }
        this.sendKeys(KeyEvent.KEYCODE_DPAD_CENTER);
    }

}
